<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.01.17
 * Time: 19:54
 */

require_once __DIR__.'/../model/singinDB.php';
require_once __DIR__.'/../view/singinV.php';

class singinController{

    function __construct()
    {
        $this->db = new singinDB();
        $this->view = new singinV();
    }

    /**
     * фотра атторизации
     * @return string
     */
    public function formSingIn(){
        $result = $this->view->getHtmlForm();

        return $result;
    }

    /**
     * авторизация пользователя
     * @param $login
     * @param $pass
     * @return string
     */
    public function singIn($login, $pass){
        if((isset($login) && $login!='') && (isset($pass) && $pass!='')){
            $user = $this->db->singinUser($login,$pass);

            //если есть такий пользователь, занесем его в сессию
            if(isset($user) && count($user)>0){
                $_SESSION["user_id"] = $user['id'];
                $_SESSION["login"] = $user['login'];
                header("Location: index.php");
                print_r('ok');
                die;
            }else{
                $result = $this->view->getHtmlError("Неверный логин или пароль");
            }
        }else{
            $result = $this->view->getHtmlError("Заполните все поля!");
        }

        return $result;
    }


    /**
     * удалим юзера из сессии
     */
    public function singOut(){
        $_SESSION["user_id"]  = '';

        header("Location: index.php");
        print_r('ok');
        die;
    }
}