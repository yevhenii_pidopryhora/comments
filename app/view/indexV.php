<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.01.17
 * Time: 16:35
 */

class indexV{

    /**
     * получить шаблон для коммента
     * @param $comment
     * @param $type
     * @param string $style
     * @return string
     */
    public function getHtml($comment, $type, $style = ''){
        $result = '';

        $result .= '
                    <div class="row block-comment" id="block-comment'.$comment['id'].'" '.$style.'>
                        <p>';

        if(isset($_SESSION['user_id']) && $_SESSION['user_id']!='' && $_SESSION['user_id']==$comment['user_id']){
            $result .= '<span><img class="img-delete" src="public/imgs/delete.png" alt="Удалить комментарий" comment_id="'.$comment['id'].'"/></span>                            
                                ';
        }

        $result .= '<span class="username-comment">'.$comment['username'].'</span>
                            <span class="userdate-comment">'.$comment['date'].'</span>';

        //рейтинг для коммента
        $result .= '<br><span class="rating">Рейтинг: '.$comment['rating'].'</span>';

        if(isset($_SESSION['user_id']) && $_SESSION['user_id']!='' && $_SESSION['user_id']!=$comment['user_id']){
            $result .= '
                                    <input class="radio-rate rate'.$comment['id'].'" name="rate" type="radio" value="1">1
                                    <input class="radio-rate rate'.$comment['id'].'" name="rate" type="radio" value="2">2
                                    <input class="radio-rate rate'.$comment['id'].'" name="rate" type="radio" value="3">3
                                    <input class="radio-rate rate'.$comment['id'].'" name="rate" type="radio" value="4">4
                                    <input class="radio-rate rate'.$comment['id'].'" name="rate" type="radio" value="5">5
                                    <input type="button" class="add-rate" comment_id="'.$comment['id'].'" value="Оценить"/>
                                    ';
        }

        $result .= '</p>
            
                        <p class="text-comment" id="text-comment'.$comment['id'].'">'.$comment['comment_text'].'</p>
                        <p>';

        if(isset($_SESSION['user_id']) && $_SESSION['user_id']!=''){
            if($type=='answer') {
                $result .= '<a class="butt-answer" for_comment="'.$comment['id'].'">Ответить</a>';
            }else{
                $result .= '<a class="edit-comment" comment_id="'.$comment['id'].'">Редактировать</a>';
            }
        }

        $result .= '</p>
                    </div>
                ';

        return $result;
    }


    /**
     * полный шаблон комментов с формой добавления
     * @param $html_comments
     * @return string
     */
    public function getHtmlBlockComments($html_comments){
        $result = '';

        //если пользователь авторизирован, и это не ajax-запрос, то вставим форму для комментов
        if((isset($_SESSION['user_id']) && $_SESSION['user_id']!='') && $_POST['ajax']!=1){
            $result.='
            <div class="row block-new-comment">
                <div class="form-group">
                    <label for="usr">Комментарий:</label>
                    <textarea class="form-control text-new-comment" id="usr" rows="3"></textarea>
                    <input type="button" class="btn btn-primary" id="add-new-comment" value="Комментарии"/>
                </div>
            </div>
            ';
        }

        if(isset($html_comments) && $html_comments!=''){

            $result .= '<div id="comments-container">';
            $result .= $html_comments;
            $result .= '</div>
                            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                            <script src="public/js/script.js" type="text/javascript"></script>
                        ';
        }

        return $result;
    }
}