<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

include_once __DIR__.'/app/controller/indexController.php';
include_once __DIR__.'/app/controller/singinController.php';

$object = new indexController();
$content = $object->getComments();

//разлогиним пользователя
if(isset($_GET['singout']) && $_GET['singout']==1){
    $object = new singinController();
    $object->singOut();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments</title>

    <link href="public/css/styles.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    

</head>
<body>

<div class="container">
    <div class="row">
        <?php
        if(isset($_SESSION['user_id']) && $_SESSION['user_id']!=''){
            echo '<a href="?singout=1" class="btn btn-primary butt-signin">Выход</a><span id="username">'.$_SESSION["login"].' / </span>';
        }else{
            echo '<a href="singin.php" class="btn btn-primary butt-signin">Вход</a>';
        }
        ?>

    </div>

    <h1>SQL-запрос на PHP</h1>
    <br>
    <p>Так как ORM слишком тяжеловесны для моих нужд, то обычно я использовал DbSimple. Однако после знакомства с Twig, которые компилирует шаблоны в php код периодически возникала идея написать что-то аналогичное для работы с БД. И вот я это сделал. На картинке представлен запрос на PHP, который после компиляции генерирует код для создания и выполнения SQL запроса.
    </p>

    <p>В первой реализации я компилировал запрос из синтаксиса аналогичного DbSimple в PHP код. Идея была в том, чтобы на выходе получить готовый код с нативными функциями без всяких оберток. При этом можно было наворачивать запросы любой сложности и скорость их разбора не влияла на время работы, так как после компиляции это был обычный нативный код. Однако сложность в отладке таких запросов (сложно было искать ошибки в синтаксисе SQL) и тот факт что время разбора запроса не так велико по сравнению с выполнением запроса привели к тому, что от идеи использовать такой подход я отказался.

    <p>Не так давно наткнулся на библиотеку по разбору PHP кода на лексемы PHP-Parser. По работе я пишу код на языке ABAP, в котором команды по работе с БД встроены в сам язык, поэтому возникла идея «А что если сделать что-то подобное для PHP»?
    </p>

    <p>Схема реализации достаточно простая и очевидная: при автозагрузке класса проверяем его наличие в директории компилированных классов. Если нет скомпилированного класса, то берем исходный файл класса, заменяем в нем все спец-команды и записываем готовый класс в директорию скомпилированных классов. Весь разбор делает библиотека PHP-Parser. Чтобы компилятор мог понять, что это именно нужная ему команда, оборачиваем все в пространство имен ML (Macro Language)
    </p>

    <?php echo $content;?>
</div>

</body>
</html>
