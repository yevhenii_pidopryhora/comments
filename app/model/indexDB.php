<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.01.17
 * Time: 16:35
 */

require_once __DIR__.'/../view/indexV.php';
include_once __DIR__."/../../include/db_connect.php";
session_start();

class indexDB{

    function __construct()
    {
        $this->view = new indexV();
    }

    /**
     * получить все комменты
     * @return resource
     */
    public function getCommentsFromDB(){
        $query = mysql_query("SELECT c.*,u.login as username FROM comments c
                                  LEFT JOIN users u ON c.user_id=u.id
                                  ORDER BY id DESC");
        //$query = mysql_query('SELECT * FROM comments');
        
        return $query;
    }

    /**
     * подсчет общего рейтинга для коммента
     * @param $comment_id
     * @return float|int
     */
    public function getRating($comment_id){
        $q = mysql_fetch_assoc(mysql_query("SELECT SUM(rating) as total, count(id) as count FROM rating WHERE comment_id=".$comment_id));
        if(isset($q['count']) && $q['count']>0){
            $rating = round($q['total']/$q['count'],1);
            $result = $rating;
        }else{
            $result = 0;
        }

        return $result;
    }

    /**
     * сохранить комментарий
     * @param $comment
     * @return bool
     */
    public function saveComment($comment, $parent_id=0){
        mysql_query("INSERT INTO comments(user_id,comment_text,parent_id,date)
                                        VALUES(
                                               '" . $_SESSION['user_id'] . "',
                                               '" . $comment . "',
                                               '" . $parent_id . "',
                                               '" . date('Y-m-d H:i:s') . "')");

        return true;
    }

    /**
     * удалить комментарий
     * @param $comment_id
     * @return bool
     */
    public function deleteComment($comment_id){
        mysql_query("DELETE FROM `comments` WHERE id=".$comment_id." AND user_id=".$_SESSION['user_id']);

        return true;
    }


    /**
     * сохранить рейтинг для коммента
     * @param $comment_id
     * @param $comment_rate
     * @return bool
     */
    public function saveRateComment($comment_id, $comment_rate){
        mysql_query("INSERT INTO rating(comment_id,user_id,rating)
                                        VALUES(
                                               '" . $comment_id . "',
                                               '" . $_SESSION['user_id'] . "',
                                               '" . $comment_rate . "')");

        return true;
    }


    /**
     * сохранить отредактированный коммент
     * @param $comment_id
     * @param $edit_comment_text
     * @return bool
     */
    public function saveEditCommentText($comment_id, $edit_comment_text){
        mysql_query("UPDATE `comments` SET `comment_text`='{$edit_comment_text}' WHERE id={$comment_id} AND user_id={$_SESSION['user_id']}");

        return true;
    }
}