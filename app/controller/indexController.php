<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 31.01.17
 * Time: 16:34
 */

require_once __DIR__.'/../model/indexDB.php';
require_once __DIR__.'/../view/indexV.php';
session_start();

class indexController
{
    function __construct()
    {
        $this->db = new indexDB();
        $this->view = new indexV();
    }

    /**
     * блок комментов
     * @return string
     */
    public function getComments(){
        $result = $this->getHtmlBlockComments();

        return $result;
    }


    /**
     * получить список отсортированных комментов
     * @param $data
     * @param int $level
     * @param int $p_counter
     * @param string $prefix
     * @return mixed
     */
    function printTree($data, $level = 0, $p_counter = 1, $prefix="") {
        $i=0;
        foreach ($data as $item) {

            if ($item['parent_id'] == 0) {
                $addr = /* $p_counter . '. ' .*/ $item['id'];
                $p_counter++;
            }

            else if ($item['parent_id'] != 0) {
                $addr =  $item['id'];
            } else {
                $addr = $item['id'];
            }

            //echo $prefix.(++$i)." ". $addr."<br>";
            global $result;
            //получить рейтинг
            $rating =  $this->db->getRating($item['id']);
            $item['rating'] = $rating;

            //подсчитаем вложеность для отступа
            $margin = '';
            if (strlen($prefix.(++$i))>1){
                $val = 40+(40*strlen($prefix.(++$i)));
                $margin = 'style="padding-left: '.$val.'px;"';
            }

            //получить шаблон для коммента
            $html = '';

            if((isset($_SESSION['user_id']) && $_SESSION['user_id']!='') && $_SESSION['user_id']==$item['user_id']){
                $html = $this->view->getHtml($item,'edit', $margin);
            }else{
                $html = $this->view->getHtml($item,'answer', $margin);
            }
            $result [] = $html;

            if (isset($item['nodes'])) {
                $this->printTree($item['nodes'], $level + 1, $p_counter + 1, $prefix.($i));
            }
        }

        return $result;
    }


    /**
     * достать все комментарии
     * @return array
     */
    function getCommentsTree()
    {
        $tree = array();

        $query = $this->db->getCommentsFromDB();
        if (! $query) return $tree;

        $nodes = array();
        $keys = array();
        while (($node = mysql_fetch_assoc($query)))
        {
            $nodes[$node['id']] =& $node; //заполняем список веток записями из БД
            $keys[] = $node['id']; //заполняем список ключей(ID)
            unset($node);
        }
        mysql_free_result($query);

        foreach ($keys as $key)
        {
            //если нашли главную ветку(или одну из главных), то добавляем её в дерево
            if ($nodes[$key]['parent_id'] === '0'){
                $tree[] =& $nodes[$key];
                //else находим родительскую ветку и добавляем текущую ветку к дочерним элементам родит.ветки.
            }else{
                if (isset($nodes[ $nodes[$key]['parent_id'] ]))
                {
                    $nodes[ $nodes[$key]['parent_id'] ]['nodes'][] =& $nodes[$key];
                }
            }
        }

        return $this->printTree($tree,0,1,"");
    }


    /**
     * шаблон с комментами
     * @return string
     */
    public function getHtmlBlockComments(){
//        $comments = $this->db->getComments();
        $comments = $this->getCommentsTree();

        global $result;
        $html_comments = implode(' ',$result);

        //получим целый блок комментов
        $result = $this->view->getHtmlBlockComments($html_comments);

        return $result;
    }


    /**
     * сохранить новый коммент
     * @param $comment_text
     */
    public function saveNewComment($comment_text){
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']!=''){
            $comment = trim($comment_text);
            $comment = addslashes(mysql_real_escape_string($comment));

            $res = $this->db->saveComment($comment);
        }

        //получим и сформируем блок комментов
        $html_comments = $this->getHtmlBlockComments();

        echo $html_comments;
    }

    /**
     * удалить коммент по id
     * @param $comment_id
     */
    public function deleteComment($comment_id){
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']!='') {
            $comment_id = mysql_real_escape_string($comment_id);
            $this->db->deleteComment($comment_id);
        }

        //получим и сформируем блок комментов
        $html_comments = $this->getHtmlBlockComments();

        echo $html_comments;
    }


    /**
     * сохранить рейтинг для коммента
     * @param $comment_id
     * @param $comment_rate
     */
    public function saveRateForComment($comment_id, $comment_rate){
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']!='') {
            $comment_id = mysql_real_escape_string($comment_id);
            $comment_rate = mysql_real_escape_string($comment_rate);
            $this->db->saveRateComment($comment_id, $comment_rate);
        }

        //получим и сформируем блок комментов
        $html_comments = $this->getHtmlBlockComments();

        echo $html_comments;
    }

    /**
     * сохранить редактированный коммент
     * @param $comment_id
     * @param $edit_comment_text
     */
    public function saveEditComment($comment_id, $edit_comment_text){
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']!='') {
            $comment_id = mysql_real_escape_string($comment_id);
            $edit_comment_text = mysql_real_escape_string(addslashes(trim($edit_comment_text)));

            $this->db->saveEditCommentText($comment_id, $edit_comment_text);
        }

        //получим и сформируем блок комментов
        $html_comments = $this->getHtmlBlockComments();

        echo $html_comments;
    }


    /**
     * Сохранить ответ
     * @param $parent_id
     * @param $answer
     */
    public function addAnswer($parent_id, $answer){
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']!='') {
            $parent_id = mysql_real_escape_string($parent_id);
            $answer = mysql_real_escape_string(addslashes(trim($answer)));

            $this->db->saveComment($answer, $parent_id);
        }

        //получим и сформируем блок комментов
        $html_comments = $this->getHtmlBlockComments();

        echo $html_comments;
    }

}