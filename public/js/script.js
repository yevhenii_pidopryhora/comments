$(document).ready(function(){

    //добавить новый коммент
    $('#add-new-comment').on('click', function(){
        var new_comment = $('.text-new-comment')[0].value.trim();

        //очистим поле для коммента
        $('.text-new-comment').val('');
        if(new_comment!=''){
            $.ajax({
                type: "POST",
                url: "/comments/app/ajax.php",
                //dataType: 'html',
                data: {'ajax':1,
                        'method':'add_comment',
                        'comment_text':new_comment},
                success: function (data) {
                    $('#comments-container').html('');
                    $('#comments-container').html(data);
                }
            });
        }
    })


    //удалить коммент по id
    $('.img-delete').on('click', function(){
        var delete_comment = confirm("Удалить комментарий?");

        if(delete_comment == true){
            var comment_id = $(this)[0].getAttribute("comment_id");
            $.ajax({
                type: "POST",
                url: "/comments/app/ajax.php",
                data: {'ajax':1,
                        'method':'delete_comment',
                        'comment_id':comment_id},
                success: function (data) {
                    $('#comments-container').html('');
                    $('#comments-container').html(data);
                }
            });
        }
    });


    //оценить коммент
    $('.add-rate').on('click', function(){
        var add_rate = confirm("Поставить оценку?");

        if(add_rate == true){
            var comment_id = $(this)[0].getAttribute("comment_id");
            //получим массив оценок для коммента
            var arr_rates = $('.rate'+comment_id);
            for (var item in arr_rates) {
                if(typeof arr_rates[item]=='object'){
                    if(arr_rates[item].checked == true){
                        arr_rates[item].checked = false;
                        var comment_rate = arr_rates[item].value;
                    }
                }
            }

            $.ajax({
                type: "POST",
                url: "/comments/app/ajax.php",
                data: {'ajax':1,
                    'method':'save_rate',
                    'comment_id':comment_id,
                    'comment_rate':comment_rate},
                success: function (data) {
                    $('#comments-container').html('');
                    $('#comments-container').html(data);
                }
            });
        }
    });


    //редактировать коммент
    $('.edit-comment').on('click', function(){
        var edit_comment = confirm("Редактировать коммент?");

        if(edit_comment==true){
            var comment_id = $(this)[0].getAttribute("comment_id");
            var comment_text = $('#text-comment'+comment_id).text();

            //скроем кнопку
            $(this)[0].style.display = "none";
            $('#text-comment'+comment_id)[0].style.display = "none";

            $(this).after('<div class="form-group edit-comment-textarea">' +
                            '<label for="usr">Комментарий:</label>' +
                            '<textarea class="form-control text-new-comment edit-comment-text'+comment_id+'" id="usr" rows="3">'+comment_text+'</textarea>' +
                            '<input type="button" class="btn btn-primary save-edit-comment" comment_id="'+comment_id+'" value="Сохранить"/>' +
                        '</div>');


            $('.save-edit-comment').on('click', function(){
                 var edit_comment_id = $(this)[0].getAttribute("comment_id");
                 var edit_comment_text = $('.edit-comment-text' + edit_comment_id)[0].value.trim();

                if(edit_comment_text!=''){
                    $.ajax({
                        type: "POST",
                        url: "/comments/app/ajax.php",
                        data: {'ajax':1,
                            'method':'edit_comment',
                            'comment_id':edit_comment_id,
                            'edit_comment_text':edit_comment_text},
                        success: function (data) {
                            $('#comments-container').html('');
                            $('#comments-container').html(data);
                        }
                    });
                }
            });
        }
    });


    //ответить на коммент
    $('.butt-answer').on('click', function(){
        var edit_comment = confirm("Ответить?");
        if(edit_comment==true){
            var for_comment = $(this)[0].getAttribute("for_comment");
            $(this)[0].style.display = "none";

            $(this).after('<div class="form-group edit-comment-textarea">' +
                '<label for="usr">Комментарий:</label>' +
                '<textarea class="form-control text-new-comment text-answer'+for_comment+'" id="usr" rows="3"></textarea>' +
                '<input type="button" class="btn btn-primary save-answer" for_comment="'+for_comment+'" value="Ответить"/>' +
                '</div>');

            $('.save-answer').on('click', function(){
                var text_answer = $('.text-answer' + for_comment)[0].value.trim();
                if(text_answer!=''){
                    $.ajax({
                        type: "POST",
                        url: "/comments/app/ajax.php",
                        data: {'ajax':1,
                            'method':'add_answer',
                            'for_comment':for_comment,
                            'text_answer':text_answer},
                        success: function (data) {
                            $('#comments-container').html('');
                            $('#comments-container').html(data);
                        }
                    });
                }
            });
        }
    });




});
