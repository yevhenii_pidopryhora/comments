<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 01.02.17
 * Time: 10:06
 */

require_once __DIR__.'/controller/indexController.php';

$object = new indexController();
    switch ($_POST['method']) {
        case 'add_comment':
            $object->saveNewComment($_POST['comment_text']);
            break;
        case 'delete_comment':
            $object->deleteComment($_POST['comment_id']);
            break;
        case 'save_rate':
            $object->saveRateForComment($_POST['comment_id'], $_POST['comment_rate']);
            break;
        case 'edit_comment':
            $object->saveEditComment($_POST['comment_id'], $_POST['edit_comment_text']);
            break;
        case 'add_answer':
            $object->addAnswer($_POST['for_comment'], $_POST['text_answer']);
            break;
    }
